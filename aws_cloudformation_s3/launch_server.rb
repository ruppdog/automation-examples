require 'aws-sdk'

templateFileLocation = "templates/"
templateFileName = "simple_s3_web_server.json"
s3TemplateFileName = templateFileName + "-" + Time.new().tv_sec.to_s
templateBucket = ENV["AWS_CLOUDFORMATION_TEMPLATE_BUCKET"]

templateFile = File.open(templateFileLocation + templateFileName)
templateBody = templateFile.read

s3Client = Aws::S3::Client.new
cloudFormationClient = Aws::CloudFormation::Client.new

# # Upload our template to s3
s3Client.put_object(bucket: templateBucket, key: s3TemplateFileName, body: templateBody)

# Get the URL for the template we just uploaded
s3Presigner = Aws::S3::Presigner.new
templateUrl = s3Presigner.presigned_url(:get_object, bucket: templateBucket, key: s3TemplateFileName)

# Build CloudFormation Stack
cloudFormationClient.create_stack({
  stack_name: "BasicWebServer",
  template_url: templateUrl,
  disable_rollback: true,
  timeout_in_minutes: 1
})

# Watch the status until this stack gets created
status = ""
while(status != "CREATE_COMPLETE") do
  resp = cloudFormationClient.list_stacks()
  status = resp.stack_summaries[0].stack_status
  if(status == "CREATE_FAILED")
    abort(status)
  end
  puts status + "... sleeping 5"
  sleep(5)
end

# Get the bucket we just created
resources = cloudFormationClient.list_stack_resources({
	stack_name: "BasicWebServer"
})
bucket = resources.stack_resource_summaries[0].physical_resource_id

# Upload index.html to our new bucket
indexFile = File.open("files/index.html")
indexBody = indexFile.read
s3Client.put_object(bucket: bucket, key: "index.html", body: indexBody)
s3Client.put_object_acl({
  acl: "public-read",
  bucket: bucket,
  key: "index.html"
})

# Output url
resp = cloudFormationClient.describe_stacks({
	stack_name: "BasicWebServer"
})
puts resp.stacks[0].outputs[0].output_value

