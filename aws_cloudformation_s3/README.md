# One Click Deploy using AWS CloudFormation

* Project: Basic S3 Web Server
* Language: Ruby
* Tools: AWS, S3, CloudFormation

## Prerequisites
##### An AWS account
##### Ruby Programming Language

##### Environment variables
In order to not commit authentication information, I required that you have your AWS credentials in your enviroment variables.  Also set your region as an environment variable and the bucket you will use to store CloudFormation templates

```
export AWS_ACCESS_KEY_ID=[AWS_ACCESS_KEY_ID]
export AWS_SECRET_ACCESS_KEY=[AWS_SECRET_ACCESS_KEY]
export AWS_REGION=[AWS_REGION]
export AWS_CLOUDFORMATION_TEMPLATE_BUCKET=[BUCKET]
```

##### Ruby Gems
This is needed in order to install bundler, which will install the other gems

* Install necessary bundler gem
```
[sudo] gem install bundler
```

* Run Bundler
```
bundle install
```

## Example Usage

* Test
```
rpsec spec
```

* Run the ruby script to bring up the stack
```
ruby launch_server.rb
```

* The output from the script is the url to the webpage