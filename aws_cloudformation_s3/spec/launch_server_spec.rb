
require 'rspec'
require 'json'
require 'aws-sdk'

describe 'basic_web_server:' do

  before(:context) do
    template_file = File.open("templates/simple_s3_web_server.json")
    @template_body = template_file.read
    @template_json = JSON.parse(@template_body)
  end

  it "has the necessary ENV variables defined" do
    expect(ENV['AWS_ACCESS_KEY_ID']).not_to be_empty
    expect(ENV['AWS_SECRET_ACCESS_KEY']).not_to be_empty
    expect(ENV['AWS_REGION']).not_to be_empty
    expect(ENV['AWS_CLOUDFORMATION_TEMPLATE_BUCKET']).not_to be_empty
  end

  it "has a valid template" do
    cloudFormationClient = Aws::CloudFormation::Client.new
    # Throws an exception if invalid
    cloudFormationClient.validate_template(template_body: @template_body)
  end

  it "has an S3 Resource in the template" do
    @hasBucket = false
    @template_json["Resources"].each do |k, resource|
      if @template_json["Resources"][k]["Type"] == "AWS::S3::Bucket"
        @hasBucket = true
      end
    end
    expect(@hasBucket).to be(true)
  end

  it "has a Public Read S3 Bucket in the template" do
    @hasPublicRead = false
    @template_json["Resources"].each do |k, resource|
      if @template_json["Resources"][k]["Type"] == "AWS::S3::Bucket"
        if @template_json["Resources"][k]["Properties"]["AccessControl"] == "PublicRead"
          @hasPublicRead = true
        end
      end
    end
    expect(@hasPublicRead).to be(true)
  end

  it "has an index.html file with the correct text" do

  end

end