#!/bin/bash

# First, let's verify that a name was passed in
if [ -z "$1" ]
then
	echo "Please specify a name for this instance";
	exit;
fi

# Next, let's verify that our AWS creds are set as env variables
if [ -z "${AWS_ACCESS_KEY_ID}" ]
then
	echo "AWS_ACCESS_KEY_ID env variable not set";
	exit;
fi

if [ -z $AWS_SECRET_ACCESS_KEY ]
then
	echo "AWS_SECRET_ACCESS_KEY env variable not set";
	exit;
fi

NODENAME=$1
REGION=us-east-1
AVAILABILITY_ZONE=us-east-1a


# We've verified that we have the arguments we need.  Let's use knife to bring this up
knife ec2 server create \
--region $REGION \
--availability-zone $AVAILABILITY_ZONE \
--image ami-60b6c60a \
--node-name $NODENAME \
--associate-public-ip \
--security-group sg-cfc408ab \
--flavor t2.micro \
--subnet subnet-ecd318c7 \
--identity-file /Users/tylerruppert/.ssh/miniproject.pem \
--aws-access-key-id $AWS_ACCESS_KEY_ID \
--aws-secret-access-key $AWS_SECRET_ACCESS_KEY \
--server-url https://ec2-54-86-53-214.compute-1.amazonaws.com/organizations/tylerruppert \
--ssh-user ec2-user \
--ssh-key miniproject

# Because of a bug in Knife.  It doesn't bootstrap correctly when creating the server.
# Get our IP so that we can bootstrap
DNSNAME=$(aws ec2 describe-instances --region $REGION --filters "Name=tag-value,Values=${NODENAME}" | grep "PublicDnsName.*ec2" | sed "s/^.*PublicDnsName.*\(ec2-[^\"]*\).*$/\1/" | sed -n 1p)

# Bootstrap because ec2 create failed to do so
knife bootstrap $DNSNAME \
--node-name $NODENAME \
--server-url https://ec2-54-86-53-214.compute-1.amazonaws.com/organizations/tylerruppert \
--identity-file /Users/tylerruppert/.ssh/miniproject.pem \
--run-list recipe[basic_web_server] \
--ssh-user ec2-user \
--sudo

