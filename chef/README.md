# One Click Deploy in Chef

This is an example of how to bring up a simple web server node using a single command with a shell script and Chef.

## Prerequisites
##### An AWS account

##### A working Chef Server
You need a chef server that is up and running.  You can find documentation on setting this up here: https://docs.chef.io/install_server.html

This can be a bit tricky to set up, and there are several gotchas.  I'll give a brief overview of how to do a very basic setup on AWS

1. Create a new ec2 instance
	For this example, we're using the Amazon Linux AMI that is at least m2.medium.  If you use a smaller instance, it can run out of memory trying to run chef.

	* I'm not going to go over the ports that need to be accessible because it depends on your setup.  You can find a list here: https://docs.chef.io/server_firewalls_and_ports.html

2. SSH into the new server, sudo to root

3. Download the Chef 12.2 rpm from http://downloads.chef.io/chef-server/ to the /tmp directory

4. Install Chef:

	```
	rpm -Uvh /tmp/chef-server-core-12.2.0-1.el6.x86_64.rpm
	```

5. Make sure your hostname is accessible from your workstation and nodes
	This is the first gotcha.  This simple setup is going to use a self-signed certificate.  You have to access the server by the same url as goes into the certificate.  If you use a gateway, then an internal IP is fine.  If you do not, then you will need an external hostname set up for your chef server.

	Check your hostname with:

	```
	hostname -f
	```

	Set your hostname with:

	```
	hostname "newvalue.com"
	```

6. Start the Server, create a user and organization: Shamelessly pulled directly from the docs

	```
	chef-server-ctl reconfigure
	chef-server-ctl user-create user_name first_name last_name email password --filename FILE_NAME
	chef-server-ctl org-create short_name "full_organization_name" --association_user user_name --filename ORGANIZATION-validator.pem
	```

7. Install the web UI
	This is the second gotcha.  The Amazon Linux AMI won't install the opscode-manage via yum.  I found the solution to this issue here: https://github.com/chef/chef-server/issues/83 .  You have to do this:

	```
	curl -s https://packagecloud.io/install/repositories/chef/stable/script.rpm.sh | bash
	yum install opscode-manage
	chef-server-ctl reconfigure
	opscode-manage-ctl reconfigure
	```

##### Chef DK & Knife
This is used on your workstation for talking to the chef server and provisioning nodes.  You can find installation instructions here: https://docs.chef.io/install_dk.html

##### AWS CLI
This is needed for a workaround I created to what appears to be a bug in Knife.  Here are the installation instructions and documentation: https://aws.amazon.com/cli/

If you are interested, here is the only other occurrence of someone else finding this bug in Knife: https://github.com/chef/knife-ec2/issues/370

##### Environment variables
In order to not commit authentication information, I required that you have your AWS credentials in your enviroment variables

```
export AWS_ACCESS_KEY_ID=[AWS_ACCESS_KEY_ID]
export AWS_SECRET_ACCESS_KEY=[AWS_SECRET_ACCESS_KEY]
```

##### Ruby Gems
This is needed in order to install bundler, and then to use berks to manage cookbooks.

## Example Usage

* Download the chef-example repository from https://bitbucket.org/ruppdog/chef-example and cd into cookbooks/basic_web_server

* Install necessary bundler gem
```
[sudo] gem install bundler
```

* Run Bundler
```
bundle install
```

* Install and upload the cookbook to your Chef Server with Berkshelf
```
berks install
berks upload
```

* Now we're ready to bring up our server!  You need to be in your home chef-example director to run this command
```
sh /path/to/launch_server.sh chef-automated-webserver-1
```

* As this script is running, it will output the public hostname of the new ec2 node.  Take note of this, as that's how we'll verify everything worked.

* Once the command finishes, navigate to the public hostname of the node in a browser.  Eureka!
```
Automation for the People
```